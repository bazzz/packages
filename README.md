# packages

Package packages is a wrapper around the system's package manager to list and update installed system packages.

## Usage

The current implementation is only for FreeBSD because I needed it for that OS. However, other systems can be implemented.