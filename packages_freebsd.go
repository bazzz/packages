package packages

import "strings"

const cmd = "/usr/local/sbin/pkg"

func list() ([]Package, error) {
	args := []string{"info"}
	out, err := execute(cmd, args...)
	if err != nil {
		return nil, err
	}
	args = []string{"version"}
	updates, err := execute(cmd, args...)

	packages := make([]Package, 0)
	for _, line := range strings.Split(out, "\n") {
		pos := strings.Index(line, " ")
		if pos < 0 {
			continue
		}
		nameversion := line[:pos]
		description := clean(line[pos:])
		dashpos := strings.LastIndex(nameversion, "-")
		title := nameversion[:dashpos]
		version := nameversion[dashpos+1:]
		update := ""
		for _, u := range strings.Split(updates, "\n") {
			pos := strings.Index(u, " ")
			if pos < 0 {
				continue
			}
			if nameversion == u[:pos] {
				update = clean(u[pos:])
				break
			}
		}
		p := Package{
			Title:       title,
			Name:        nameversion,
			Description: description,
			Version:     version,
			Update:      update,
		}
		packages = append(packages, p)
	}

	return packages, nil
}
