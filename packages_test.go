package packages

import "testing"

func TestList(t *testing.T) {
	packages, err := List()
	if err != nil {
		t.Fatal(err)
	}
	for _, p := range packages {
		t.Log("Package:", p)
	}
}
