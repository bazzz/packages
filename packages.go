package packages

import (
	"fmt"
	"os/exec"
	"strings"
)

type Package struct {
	Title       string
	Name        string
	Description string
	Version     string
	Update      string
}

func List() ([]Package, error) {
	return list()
}

func execute(cmd string, args ...string) (string, error) {
	out, err := exec.Command(cmd, args...).CombinedOutput()
	if err != nil {
		return "", fmt.Errorf("cannot execute command '%v %v': %w", cmd, args, err)
	}
	return string(out), nil
}

func clean(input string) string {
	return strings.Trim(input, " \t\n")
}
